package com.shukur.lesson6.task.classroom;

import java.lang.reflect.Field;

public class Reflection {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException {

        Class<?> class1 = Class.forName("com.shukur.lesson6.task.classroom.Welcome");
        Class<?> cls = Welcome.class;
        System.out.println(class1);
        System.out.println(cls);
        Field[] fields = cls.getDeclaredFields();
        System.out.println("------------------------------------");
        for (Field field : fields) {
            field.setAccessible(true);
            System.out.println(field.getType().getName());
            if (field.getType().getName().equals("java.lang.String")) {
                String stringFieldValue = (String) field.get(new Welcome());
                System.out.println(stringFieldValue);
            } else if (field.getType().getName().equals("java.lang.Integer")) {
                Integer intFieldValue = (Integer) field.get(new Welcome());
                System.out.println(intFieldValue);
            }
        }
    }
}
