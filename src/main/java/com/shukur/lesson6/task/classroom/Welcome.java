package com.shukur.lesson6.task.classroom;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class Welcome {
    String text = "Hello admin";
    Integer age = 20;
}
